package Math.Brackets is
	pragma Pure;

	type Bracket_Index is (Lower, Upper);

	type Real_Bracket is array (Bracket_Index) of Float;

end Math.Brackets;