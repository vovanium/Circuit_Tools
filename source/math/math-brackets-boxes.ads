package Math.Brackets.Boxes is
	pragma Pure;

	type Real_Box is array (Integer range <>) of Real_Bracket;

end Math.Brackets.Boxes;
