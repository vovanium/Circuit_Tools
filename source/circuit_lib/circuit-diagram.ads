with Ada.Containers.Indefinite_Vectors;
with Ada.Numerics.Real_Arrays;
with Math.Brackets.Boxes;
package Circuit.Diagram is

	subtype Coordinate is Float; -- Graphic coordinate.
	-- Zero corresponds to basic modular grid (?)
	-- May be will be replaced with fixed width type

	subtype Coord_Vector is Ada.Numerics.Real_Arrays.Real_Vector (1 .. 2);
	-- Vectors for graphic data

	subtype Coord_Box is Math.Brackets.Boxes.Real_Box (1 .. 2);
	-- Coordinate boundary box

	type Shape is interface; -- The basic graphic entity

	package Shape_Vectors is new Ada.Containers.Indefinite_Vectors (
		Index_Type => Positive,
		Element_Type => Shape'Class,
		"=" => "="
	);

	type Shape_Group is new Shape with record
		Data : Shape_Vectors.Vector;
	end record;

end Circuit.Diagram;
