with Serialization.XML.Writers;

package Circuit.Diagram.IO.SVG is

	type SVG_Saver is limited new Shape_Saver with record
		Stream : Serialization.XML.Writers.XML_Writer;
	end record;

	procedure Create_File (
		Stream : in out SVG_Saver;
		Name : String);

	procedure Save (
		Stream : in out SVG_Saver;
		Data   : Circuit.Diagram.Shape_Group'Class);

end Circuit.Diagram.IO.SVG;
