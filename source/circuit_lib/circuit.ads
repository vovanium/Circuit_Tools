with Ada.Strings.Unbounded;
use Ada.Strings.Unbounded;

package Circuit is

	subtype Common_String is Unbounded_String;
	-- String used thoughout the project
	
	use type Common_String;
	function To_Common_String (Source : String) return Common_String
		renames To_Unbounded_String;

end Circuit;