package Circuit.Diagram.IO is

	type Shape_Loader is limited interface;

	function Load (File : Shape_Loader) return Shape_Group'Class is abstract;

	type Shape_Saver is limited interface;

	procedure Save (
		File : in out Shape_Saver;
		Data : in Shape_Group'Class) is null;

	type Shape_IO is limited interface and Shape_Loader and Shape_Saver;

	type Null_Shape_IO is new Shape_IO with null record;

	function Load (File : Null_Shape_IO) return Shape_Group'Class;

end Circuit.Diagram.IO;
