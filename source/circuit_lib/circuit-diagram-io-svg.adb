with Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;
with Serialization.XML;
use  Serialization.XML;
with Serialization.XML.Writers;
use  Serialization.XML.Writers;

package body Circuit.Diagram.IO.SVG is

	procedure Create_File (Stream : in out SVG_Saver; Name : String) is
	begin
		Create_File (Stream.Stream, Name);
	end Create_File;

	procedure Save (
		Stream : in out SVG_Saver;
		Data   : Circuit.Diagram.Shape_Group'Class
	) is
	--	Attr : Attribute_Maps.Map;
	begin
		-- Prolog
		Put_Tag_Start (Stream.Stream, "xml", Question);
		Put_Tag_Attribute (Stream.Stream, "version", "1.0");
		Put_Tag_Attribute (Stream.Stream, "encoding", "UTF-8");
		Put_Tag_Attribute (Stream.Stream, "standalone", "no");
		Put_Tag_End (Stream.Stream, Question);

		Put_Tag_Start (Stream.Stream, "svg", Opening);
		Put_Tag_Attribute (Stream.Stream, "xmlns:svg", "http://www.w3.org/2000/svg");
		Put_Tag_Attribute (Stream.Stream, "xmlns ", "http://www.w3.org/2000/svg");
		--Put_Line(Stream.Stream.File, " xmlns:ei=" &
		-- """http://git.qoto.org/vovanium/Circuit_Tools""");
		Put_Tag_Attribute (Stream.Stream, "width", "20mm");
		Put_Tag_Attribute (Stream.Stream, "height", "10mm");
		Put_Tag_Attribute (Stream.Stream, "viewBox", "0 -10 20 10");
		Put_Tag_Attribute (Stream.Stream, "version", "1.1");
		Put_Tag_End (Stream.Stream, Opening);

		Put_Tag_Start (Stream.Stream, "style", Opening);
		Put_Tag_Attribute (Stream.Stream, "type", "text/css");
		Put_Tag_End (Stream.Stream, Opening);

		Put_Line (Stream.Stream, "* {fill:none;stroke:#000000;" &
		 "stroke-width:0.2;stroke-linecap:round;" &
		 "stroke-linejoin:round;stroke-miterlimit:4;" &
		 "stroke-opacity:1;stroke-dasharray:none}");

		Put_Tag (Stream.Stream, "style", Closing);

		Put_Tag_Start (Stream.Stream, "rect", Alone);
		Put_Tag_Attribute (Stream.Stream, "id", "rect2818");
		Put_Tag_Attribute (Stream.Stream, "width", "10");
		Put_Tag_Attribute (Stream.Stream, "height", "4");
		Put_Tag_Attribute (Stream.Stream, "x", "5");
		Put_Tag_Attribute (Stream.Stream, "y", "-7");
		Put_Tag_End (Stream.Stream, Alone);

		Put_Tag_Start (Stream.Stream, "path", Alone);
		Put_Tag_Attribute (Stream.Stream, "d", "m 0,-5 5,0");
		Put_Tag_End (Stream.Stream, Alone);

		Put_Tag_Start (Stream.Stream, "path", Alone);
		Put_Tag_Attribute (Stream.Stream, "d", "m 15,-5 5,0");
		Put_Tag_End (Stream.Stream, Alone);

		-- Epilog
		Put_Tag (Stream.Stream, "svg", Closing);
	end Save;

end Circuit.Diagram.IO.SVG;
