with Ada.Strings.Unbounded, Ada.Text_IO, Ada.Integer_Text_IO;
use  Ada.Strings.Unbounded, Ada.Text_IO, Ada.Integer_Text_IO;
with Ada.Command_Line;
use  Ada.Command_line;
with Circuit.Diagram, Circuit.Diagram.IO;
with Circuit.Diagram.IO.SVG;
with Serialization.XML.Writers;
use Serialization.XML.Writers;

procedure Test is
	NIO: Circuit.Diagram.IO.Null_Shape_IO;
	X: Circuit.Diagram.Shape_Group'Class := NIO.Load;
	SVGIO: Circuit.Diagram.IO.SVG.SVG_Saver;
begin
	SVGIO.Create_File("example.svg");
	SVGIO.Save(X);
end Test;