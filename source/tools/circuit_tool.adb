with Ada.Strings.Unbounded, Ada.Text_IO, Ada.Integer_Text_IO;
use  Ada.Strings.Unbounded, Ada.Text_IO, Ada.Integer_Text_IO;
with Ada.Command_Line;
use  Ada.Command_line;

procedure Circuit_Tool is
	
begin
	Put("Command: ");
	Put(Command_Name);
	New_Line;
	for I in 1 .. Argument_Count loop
		Put("Argument: ");
		Put(Argument(I)'Length);
		Put(" ");
		Put(Argument(I));
		New_Line;
	end loop;
end Circuit_Tool;