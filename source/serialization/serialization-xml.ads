with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;
--with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Text_IO;
use  Ada.Text_IO;

package Serialization.XML with Spark_Mode is

	subtype XML_String is String;

--	package Attribute_Maps is new Ada.Containers.Indefinite_Ordered_Maps (
--		Key_Type => String,
--		Element_Type => String,
--		"<" => "<",
--		"=" => "="
--	);

	type Tag_Mode is (Alone, Opening, Closing, Question);

end Serialization.XML;
