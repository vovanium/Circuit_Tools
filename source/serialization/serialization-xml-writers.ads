with Ada.Text_IO;
use Ada.Text_IO;

package Serialization.XML.Writers is

	type XML_Put_State is record
		Indent : Natural := 0;
	end record;

	type XML_Writer is limited record
		File  : File_Type;
		State : XML_Put_State;
	end record;

	procedure Create_File (
		Stream : in out XML_Writer;
		Name   : String);

	procedure Put_Line (
		Stream : XML_Writer;
		Text   : String);

	procedure Put_Tag_Start (
		Stream : in out XML_Writer;
		Tag    : String;
		Mode   : Tag_Mode);

	procedure Put_Tag_End (
		Stream : in out XML_Writer;
		Mode   : Tag_Mode);

	function Escape (Value : String) return XML_String;

	function Attribute (Key, Value : String) return XML_String
	is (
		Key & "=""" & Escape (Value) & """"
	);

	procedure Put_Tag_Attribute (
		Stream     : XML_Writer;
		Key, Value : String);

	procedure Put_Tag (
		Stream : in out XML_Writer;
		Tag    : String;
		Mode   : Tag_Mode);

end Serialization.XML.Writers;
