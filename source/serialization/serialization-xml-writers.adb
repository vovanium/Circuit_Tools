package body Serialization.XML.Writers is

	procedure Create_File (
		Stream : in out XML_Writer;
		Name   : String
	) is
	begin
		Create (Stream.File, Out_File, Name);
	end Create_File;

	procedure Put_Indent (
		Stream : XML_Writer
	) is
	begin
		for I in 1 .. Stream.State.Indent loop
			Put (Stream.File, "  ");
		end loop;
	end Put_Indent;

	procedure Put_Line (
		Stream : XML_Writer;
		Text   : String
	) is
	begin
		Put_Indent (Stream);
		Put_Line (Stream.File, Text);
	end Put_Line;

	procedure Put_Tag_Start (
		Stream : in out XML_Writer;
		Tag    : String;
		Mode   : Tag_Mode
	) is
	begin
		if Mode = Closing then
			Stream.State.Indent := Stream.State.Indent - 1;
		end if;

		Put_Indent (Stream);

		case Mode is
		when Closing =>
			Put (Stream.File, "</");
		when Question =>
			Put (Stream.File, "<?");
		when others =>
			Put (Stream.File, "<");
		end case;

		Put (Stream.File, Tag);

	end Put_Tag_Start;

	procedure Put_Tag_End (
		Stream : in out XML_Writer;
		Mode   : Tag_Mode
	) is
	begin
		case Mode is
		when Alone =>
			Put (Stream.File, "/");
		when Question =>
			Put (Stream.File, "?");
		when others =>
			null;
		end case;

		Put (Stream.File, ">");

		if Mode = Opening then
			Stream.State.Indent := Stream.State.Indent + 1;
		end if;
	
		New_Line (Stream.File);
	end Put_Tag_End;

	function Escape (Value : String) return XML_String is
		L : Natural := 0; -- String length
		K : Natural := 0;
		function Esc (C : Character) return String is (
			case C is
				when '"' => "&quot;",
				when others => "" & C
		) with Post => Esc'Result'Length in 1 .. 6;
	begin
		for I of Value loop
			L := L + Esc (I)'Length;
		end loop;
		return Result : XML_String (1 .. L) do
			for I of Value loop
				declare
					E : constant String := Esc (I);
				begin
					Result (K + 1 .. K + E'Length) := E;
					K := K + E'Length;
				end;
			end loop;
			pragma Assert (K = L); -- result string filled
		end return;
	end Escape;

	procedure Put_Tag_Attribute (
		Stream : XML_Writer;
		Key, Value : String
	) is
	begin
		Put (Stream.File, " " & Key & "=""" &
		 Escape (Value) & """");
	end Put_Tag_Attribute;

	procedure Put_Tag (
		Stream : in out XML_Writer;
		Tag    : String;
		Mode   : Tag_Mode
	) is
	begin
		Put_Tag_Start (Stream, Tag, Mode);
		Put_Tag_End (Stream, Mode);
	end Put_Tag;

end Serialization.XML.Writers;
